import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import { S3 } from 'aws-sdk';
import { XMLParser } from 'fast-xml-parser';
import { HandbookPage } from './interfaces/handbook-page';
import { URL, parse } from 'url';
import { randomUUID } from 'crypto';

const bucketName = "handbook-prod";
const s3 = new S3();
const parser = new XMLParser();

/**
 *
 * Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format
 * @param {Object} event - API Gateway Lambda Proxy Input Format
 *
 * Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
 * @returns {Object} object - API Gateway Lambda Proxy Output Format
 *
 */

export const lambdaHandler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    let requestURL = event.queryStringParameters?.url;
    if(!requestURL || !stringIsAValidUrl(requestURL)) {
        return {
            statusCode: 500,
            body: JSON.stringify({
                "id": randomUUID(),
                "type": "danger",
                "icon": "fa-solid fa-warn",
                "title": "Error: No URL parameter",
                "messageHTML": "<p>You need to specify a valid URL parameter</p>",
                "autohide": true
            }),
        };
    }
    // remove anchors from url
    if(requestURL.includes('#'))
        requestURL = requestURL.split("#")[0];
    // make sure url ends with '/'
    if(!requestURL.endsWith('/'))
        requestURL = requestURL + "/";
    try {
        const output = await s3
            .getObject({
                Bucket: bucketName,
                Key: "sitemap.xml"
            })
            .promise();
        const page: HandbookPage = parseXML(output.Body?.toString() || "", requestURL);
        if(page) {
            return {
                statusCode: 200,
                body: JSON.stringify(page)
            };
        }
        return {
            statusCode: 404,
            body: JSON.stringify({
                    "id": randomUUID(),
                    "type": "warning",
                    "icon": "fa-solid fa-warn",
                    "title": "Error: Page not found",
                    "messageHTML": "<p>This page is not found in the site</p>",
                    "autohide": true
                }),
        };
    } catch (err) {
        console.log("Error:" + err);
        return {
            statusCode: 500,
            body: JSON.stringify({
                "id": randomUUID(),
                "type": "danger",
                "icon": "fa-solid fa-warn",
                "title": "Error: Unknown Server Error",
                "messageHTML": "<p>An unknown server occured</p>",
                "autohide": true
            }),
        };
    }
};

function parseXML(xml: string, pageurl: string): HandbookPage | undefined {
    const json = parser.parse(xml);
    const urls = json.urlset.url;
    console.log(urls[0]);
    for (let i = 0; i < urls.length; i++) {
        const item = urls[i];
        const url = item.loc || "";
        const lastmod = item.lastmod || "";
        if(url === pageurl) {
            let page : HandbookPage = {
                url: url,
                lastupdated: lastmod
            }
            return page
        }
    }
}

function stringIsAValidUrl(s: string): boolean {
    try {
        new URL(s);
        const parsed = parse(s);
        const protocols = ["https"];
        return protocols
            ? parsed.protocol
                ? protocols.map(x => `${x.toLowerCase()}:`).includes(parsed.protocol)
                : false
            : true;
    } catch (err) {
        return false;
    }
};
