export interface HandbookNotification {
    id:                 string;
    type:               string;
    icon:               string;
    title:              string;
    messageHTML:        string;
    posted:             Date;
    expires:            Date;
    autohide:           boolean;
    url?:                string;
}

export interface HandbookNotifications {
    notifications: HandbookNotification[];
}