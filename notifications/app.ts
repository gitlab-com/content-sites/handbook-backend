import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import { S3 } from 'aws-sdk';
import { HandbookNotification, HandbookNotifications } from './interfaces/handbook-notification';

const bucketName = "handbook-prod";
const s3 = new S3();

/**
 *
 * Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format
 * @param {Object} event - API Gateway Lambda Proxy Input Format
 *
 * Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
 * @returns {Object} object - API Gateway Lambda Proxy Output Format
 *
 */

export const lambdaHandler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    try {
        const output = await s3
            .getObject({
                Bucket: bucketName,
                // Key is file name in AWS terminology
                Key: "notifications.json",
            })
            .promise();
        const allhandbookNotifications = JSON.parse(output.Body?.toString() || "") as HandbookNotifications;
        let handbookNotification = allhandbookNotifications.notifications.filter(notification => notification.url === undefined);
        if(event.queryStringParameters?.path) {
            const path = event.queryStringParameters.path;
            handbookNotification = handbookNotification.concat(allhandbookNotifications.notifications.filter(notification => notification.url === path));
        }        
        return {
            statusCode: 200,
            body: JSON.stringify(handbookNotification),
        };
    } catch (err) {
        console.log("Error:" + err);
        return {
            statusCode: 500,
            body: JSON.stringify({
                message: 'some error happened',
            }),
        };
    }
};
